﻿1
00:00:06,000 --> 00:00:12,074
മലയാളം സബ്ടൈറ്റിലിന് സന്ദര്‍ശിക്കുക
http://www.malayalamsubtitles.org/

2
00:02:34,489 --> 00:02:36,116
Teacher

3
00:02:38,952 --> 00:02:43,457
I see your talents have gone
beyond the mere physical level.

4
00:02:43,623 --> 00:02:47,711
Your skills are now at the point
of spiritual insight.

5
00:02:48,462 --> 00:02:52,299
I have questions: what is the highest
technique you hope to achieve?

6
00:02:52,466 --> 00:02:55,135
- To have no technique.
- Very good.

7
00:02:55,302 --> 00:02:57,804
What are your thoughts
when facing an opponent?

8
00:02:57,971 --> 00:02:59,681
There is no opponent.

9
00:02:59,848 --> 00:03:01,391
And why is that?

10
00:03:01,558 --> 00:03:05,687
Because the word "I" does not exist.

11
00:03:05,854 --> 00:03:09,316
So. Continue.

12
00:03:10,192 --> 00:03:13,362
A good fight should be...

13
00:03:13,820 --> 00:03:19,076
...like a small play, but played seriously.

14
00:03:19,618 --> 00:03:24,706
A good martial artist
does not become tense, but ready.

15
00:03:24,956 --> 00:03:28,335
Not thinking, yet not dreaming.

16
00:03:28,502 --> 00:03:31,004
Ready for whatever may come.

17
00:03:31,296 --> 00:03:34,132
When the opponent expand, I contract.

18
00:03:34,299 --> 00:03:36,676
When he contracts, I expand.

19
00:03:36,843 --> 00:03:39,429
And when there is an opportunity...

20
00:03:40,472 --> 00:03:42,724
...I do not hit.

21
00:03:44,059 --> 00:03:46,603
It hits all by itself.

22
00:03:46,770 --> 00:03:49,356
Now, you must remember...

23
00:03:49,523 --> 00:03:53,527
...the enemy has only images
and illusions...

24
00:03:53,693 --> 00:03:57,197
...behind which he hides his true motives.

25
00:03:57,364 --> 00:04:01,034
Destroy the image
and you will break the enemy.

26
00:04:01,201 --> 00:04:06,706
The "it" that you refer to
is a powerful weapon...

27
00:04:06,873 --> 00:04:13,004
...easily misused by the martial artist
who deserts his vows.

28
00:04:13,338 --> 00:04:18,510
For centuries now, the code
of the Shaolin Temple has been preserved.

29
00:04:18,677 --> 00:04:24,349
Remember, the honor of our brotherhood
has been held true.

30
00:04:25,392 --> 00:04:29,521
Tell me now the Shaolin commandment
number 13.

31
00:04:30,105 --> 00:04:33,692
"A martial artist has to take responsibility
for himself...

32
00:04:33,859 --> 00:04:37,612
...and accept the consequences
of his own doing."

33
00:04:41,116 --> 00:04:43,243
I'm ashamed to tell you now...

34
00:04:43,410 --> 00:04:47,122
...among all the Shaolin men
I have taught...

35
00:04:47,622 --> 00:04:51,501
...there is one who has turned
the ways of knowledge and strength...

36
00:04:51,668 --> 00:04:54,504
...to his own base ends.

37
00:04:55,422 --> 00:04:59,593
He has perverted all we hold sacred.

38
00:05:00,260 --> 00:05:02,804
His name is Han.

39
00:05:03,388 --> 00:05:07,017
In defiance of all our beliefs...

40
00:05:09,394 --> 00:05:13,148
...he has brought disgrace
to the Shaolin Temple.

41
00:05:13,982 --> 00:05:19,112
So it is now for you to reclaim
our lost honor.

42
00:05:19,279 --> 00:05:22,407
Yes. I understand.

43
00:05:22,574 --> 00:05:26,578
There is a man here.
You will go to him.

44
00:05:34,252 --> 00:05:37,005
Hello, Mr. Lee.
My name's Braithwaite.

45
00:05:37,172 --> 00:05:39,299
Hello, Mr. Braithwaite.

46
00:05:39,633 --> 00:05:43,845
I've come to speak to you
about a matter of great importance.

47
00:05:44,054 --> 00:05:46,973
- Have some tea?
- Yes, indeed.

48
00:05:50,310 --> 00:05:52,437
This is very pleasant.

49
00:06:00,612 --> 00:06:04,991
Mr. Lee, I've come to speak to you
about a tournament of martial arts.

50
00:06:05,283 --> 00:06:08,119
A tournament to which
you've received an invitation.

51
00:06:08,828 --> 00:06:12,374
Specifically, a tournament
organized by Mr. Han.

52
00:06:14,042 --> 00:06:17,087
- Han's tournament.
- I know, I know, I know.

53
00:06:17,254 --> 00:06:21,758
But we'd very much like you to attend
that particular tournament, Mr. Lee.

54
00:06:22,259 --> 00:06:24,678
"We," Mr. Braithwaite?

55
00:06:26,721 --> 00:06:28,848
It's Lao's time.

56
00:06:29,015 --> 00:06:31,017
Yes, of course.

57
00:06:43,196 --> 00:06:44,823
Kick me.

58
00:06:46,575 --> 00:06:48,118
Kick me.

59
00:06:56,501 --> 00:06:59,004
What was that? An exhibition?

60
00:06:59,296 --> 00:07:03,717
We need emotional content.

61
00:07:05,343 --> 00:07:07,053
Try again.

62
00:07:18,148 --> 00:07:22,569
I said, emotional content, not anger.

63
00:07:22,861 --> 00:07:26,406
Now, try again. With me.

64
00:07:36,708 --> 00:07:38,501
That's it.

65
00:07:39,252 --> 00:07:41,087
How did it feel to you?

66
00:07:43,590 --> 00:07:45,925
Let me think...

67
00:07:46,092 --> 00:07:49,387
Don't think. Feel.

68
00:07:49,554 --> 00:07:53,975
It is like a finger pointing
a way to the moon.

69
00:07:55,018 --> 00:07:57,687
Don't concentrate on the finger...

70
00:07:57,854 --> 00:08:02,275
...or you will miss all that heavenly glory.

71
00:08:03,193 --> 00:08:05,320
Do you understand?

72
00:08:09,115 --> 00:08:14,537
Never take your eyes off your opponent,
even when you bow.

73
00:08:15,747 --> 00:08:17,791
That's it.

74
00:10:40,767 --> 00:10:42,268
There.

75
00:10:42,769 --> 00:10:45,939
That's Han. That's the only film
we have on him.

76
00:10:46,105 --> 00:10:48,191
We know he was a member
of your temple.

77
00:10:48,358 --> 00:10:52,195
A Shaolin monk, now a renegade.

78
00:10:53,321 --> 00:10:56,699
That's Oharra behind him.
Personal bodyguard.

79
00:10:56,866 --> 00:11:01,120
Tough, ruthless, as you might expect,
being Han's bodyguard.

80
00:11:02,247 --> 00:11:05,416
We got our hands
on a demonstration film of Oharra.

81
00:11:05,583 --> 00:11:09,796
All real bricks and boards,
nothing phony about any of it.

82
00:11:09,963 --> 00:11:13,925
This was before he picked up
a facial scar somewhere.

83
00:11:24,477 --> 00:11:26,938
This is where you'll be going.

84
00:11:27,397 --> 00:11:30,108
An island fortress, really.

85
00:11:30,275 --> 00:11:36,030
After the war, the nationality of the island
was uncertain.

86
00:11:36,197 --> 00:11:39,909
And sometime after that, Han bought it.

87
00:11:40,076 --> 00:11:41,661
What do you know about Han?

88
00:11:42,161 --> 00:11:44,205
He lives like a king on that island.

89
00:11:44,372 --> 00:11:47,208
Totally self-sufficient.

90
00:11:47,375 --> 00:11:49,168
All of his efforts, seemingly...

91
00:11:49,335 --> 00:11:55,425
...are directed toward supporting
what he calls his "school of martial arts."

92
00:11:55,592 --> 00:11:58,928
Han's only contact with the outside world
is this tournament...

93
00:11:59,095 --> 00:12:00,972
...which he holds every three years.

94
00:12:02,557 --> 00:12:06,853
<i>This was a stewardess, Mary King,
found floating in the harbor.</i>

95
00:12:07,020 --> 00:12:09,689
<i>Nothing unusual about a body
in the harbor.</i>

96
00:12:09,856 --> 00:12:14,027
<i>But this girl was last seen at a party
aboard Han's private junk.</i>

97
00:12:14,193 --> 00:12:17,363
<i>They'd reported her lost at sea
before the body was found.</i>

98
00:12:18,031 --> 00:12:20,158
<i>We believe he selects attractive girls...</i>

99
00:12:20,366 --> 00:12:23,036
<i>...methodically builds
their dependence on drugs...</i>

100
00:12:23,202 --> 00:12:26,664
<i>...then sells them to an elite clientele
around the world.</i>

101
00:12:26,831 --> 00:12:29,709
<i>What did the autopsy reveal
as the cause of death?</i>

102
00:12:29,876 --> 00:12:33,046
<i>- She did not drown.
- She OD'd?</i>

103
00:12:38,676 --> 00:12:43,556
Yes. Cause of death was heroin overdose.

104
00:12:43,723 --> 00:12:46,476
You still don't have enough
to bust up his operation.

105
00:12:47,143 --> 00:12:50,939
We know everything.
We can prove nothing.

106
00:12:52,565 --> 00:12:55,026
We want you to go in there as our agent.

107
00:12:55,193 --> 00:12:58,696
- Get us our evidence.
- And get out in one piece to give it to you.

108
00:13:00,031 --> 00:13:02,575
We'll give you anything you need.

109
00:13:02,742 --> 00:13:05,370
Electronic equipment, weapons, anything.

110
00:13:05,536 --> 00:13:08,164
- Drink?
- No, thanks.

111
00:13:08,331 --> 00:13:09,707
Guns.

112
00:13:09,874 --> 00:13:14,045
Now, why doesn't somebody
pull out a .45 and bang, settle it?

113
00:13:14,212 --> 00:13:16,506
No. No guns.

114
00:13:17,215 --> 00:13:19,550
Look at this map here.

115
00:13:20,885 --> 00:13:25,974
As you know, the possession of a weapon
is a serious offense here.

116
00:13:26,140 --> 00:13:31,980
Han's island rests partly
within our territorial waters.

117
00:13:32,146 --> 00:13:35,900
If we had the slightest reason to believe
he has any kind of arsenal...

118
00:13:36,067 --> 00:13:37,527
...we'd move in on him.

119
00:13:37,694 --> 00:13:41,406
Besides, Han would never allow guns
on the island anyway.

120
00:13:41,572 --> 00:13:46,119
He had a bad experience with them once
and he's fearful of assassination.

121
00:13:46,285 --> 00:13:50,707
You can't really blame him.
Any bloody fool can pull a trigger.

122
00:13:51,040 --> 00:13:53,209
I guess I won't need anything.

123
00:13:53,876 --> 00:13:56,671
There's a radio on the island.
We'll monitor it...

124
00:13:56,838 --> 00:13:58,756
...on the chance you can get to it.

125
00:13:58,923 --> 00:14:00,341
And then you come?

126
00:14:01,718 --> 00:14:03,761
Someone will.

127
00:14:03,928 --> 00:14:06,097
We aren't an agency of enforcement.

128
00:14:06,347 --> 00:14:10,435
We function as gatherers
of information, evidence.

129
00:14:10,601 --> 00:14:13,855
Upon which interested
governments can act.

130
00:14:14,022 --> 00:14:18,568
I see. If there's any trouble,
you make a phone call.

131
00:14:18,735 --> 00:14:20,945
Oh, by the way, two months ago...

132
00:14:21,112 --> 00:14:25,533
...we managed to place a female operative
on the island.

133
00:14:25,700 --> 00:14:27,869
Since then, we've lost her.

134
00:14:28,244 --> 00:14:30,455
If she's there, she might have something.

135
00:14:30,830 --> 00:14:33,207
Name's Mei Ling.

136
00:14:33,374 --> 00:14:37,545
- Sure you won't have one?
- No, thanks.

137
00:15:16,167 --> 00:15:20,671
But now the time has come to tell you
something very difficult.

138
00:15:21,172 --> 00:15:25,927
I'm happy you have decided to go
to Han's tournament.

139
00:15:26,260 --> 00:15:28,096
Yeah.

140
00:15:28,596 --> 00:15:33,226
The last of the tournaments
were held three years ago.

141
00:15:33,392 --> 00:15:38,564
I was in the city with your sister
at that time.

142
00:15:40,775 --> 00:15:42,985
I didn't know that.

143
00:15:44,112 --> 00:15:45,363
Yes.

144
00:15:46,155 --> 00:15:49,158
<i>Many of Han's men
had come in from the island.</i>

145
00:15:49,325 --> 00:15:53,704
<i>They were everywhere,
bullying and arrogant.</i>

146
00:15:53,871 --> 00:15:57,041
<i>We were on our way into town.</i>

147
00:16:23,943 --> 00:16:25,236
Stay back!

148
00:16:29,824 --> 00:16:31,242
Run! Run!

149
00:16:32,577 --> 00:16:34,245
Now!

150
00:19:56,280 --> 00:19:59,033
Now you know the truth.

151
00:19:59,617 --> 00:20:01,619
When you get to the city...

152
00:20:01,786 --> 00:20:06,791
...pay your respects to your sister
and your mother.

153
00:20:11,462 --> 00:20:13,631
I will, old man.

154
00:20:32,483 --> 00:20:35,569
You will not agree with
what I am going to do.

155
00:20:37,029 --> 00:20:40,658
It is contrary to all
that you have taught me...

156
00:20:43,828 --> 00:20:46,831
...and all that Su Lin believed.

157
00:20:55,047 --> 00:20:57,341
I must leave.

158
00:21:00,928 --> 00:21:04,348
Please try to find a way to forgive me.

159
00:22:09,288 --> 00:22:11,123
Double or nothing?

160
00:22:12,249 --> 00:22:14,418
That's about 1000 bucks a foot, Roper.

161
00:22:16,587 --> 00:22:18,422
Why not?

162
00:22:34,897 --> 00:22:39,610
I'm sorry, Mr. Roper isn't in right now.
May I take a message?

163
00:23:02,633 --> 00:23:05,427
That's a tough shot, Mr. Roper.

164
00:23:09,640 --> 00:23:11,809
Excuse me, fellas.

165
00:23:12,268 --> 00:23:15,229
- I say he can't make it.
- What'll you bet?

166
00:23:16,480 --> 00:23:17,898
You gotta love him.

167
00:23:18,065 --> 00:23:23,195
Come on, Roper.
It's 175 big ones by Monday the 15th.

168
00:23:23,362 --> 00:23:25,614
Only 150.

169
00:23:26,448 --> 00:23:28,242
You forget interest.

170
00:23:28,659 --> 00:23:32,663
- Maybe I ought to talk to Freddie.
- You take advantage, Roper.

171
00:23:33,330 --> 00:23:34,665
Come on, fellas.

172
00:23:34,832 --> 00:23:37,668
It's the dough, Roper,
or we gotta break something.

173
00:23:39,378 --> 00:23:40,796
You got it?

174
00:23:43,632 --> 00:23:45,843
Freddie says this is for your own good.

175
00:24:09,408 --> 00:24:12,494
Better confirm that flight
to Hong Kong for me.

176
00:24:13,871 --> 00:24:19,043
- How much do I have left in the bank?
-$63.43.

177
00:24:19,209 --> 00:24:22,463
- It's all yours.
- Thanks.

178
00:24:22,630 --> 00:24:26,091
- Only, I think you'll need it.
- You wanna bet?

179
00:25:58,308 --> 00:26:00,477
Double-punching. Ready...

180
00:26:32,801 --> 00:26:35,471
Going on a trip, are you?

181
00:26:42,770 --> 00:26:47,441
- Hey, this jig's got a passport.
- Where you going, jig?

182
00:26:49,026 --> 00:26:53,697
- Where's the plane ticket for?
- Hong Kong, via Hawaii.

183
00:26:54,281 --> 00:26:56,116
He's not going to Hawaii.

184
00:26:58,869 --> 00:27:01,622
Well, look what we got here!

185
00:27:01,789 --> 00:27:04,500
Assaulting a police officer.

186
00:27:54,299 --> 00:27:58,053
- Hey, soldier, shape it up!
- Roper! Hey, how are you, man?

187
00:27:58,220 --> 00:28:02,266
How am I, man? How am I?
I'm glad to see you, that's how I am.

188
00:28:02,432 --> 00:28:06,103
- Hey, how long has it been? Five...?
- Six years, man.

189
00:28:06,270 --> 00:28:08,105
- Six.
- It's not as long as it seems.

190
00:28:08,272 --> 00:28:09,731
Yeah, I know what you mean.

191
00:28:09,898 --> 00:28:13,694
- What you been doing since Nam?
- Hanging in a little bit.

192
00:28:13,861 --> 00:28:15,946
- All these yours?
- Always first-class.

193
00:28:16,113 --> 00:28:17,781
Same old Roper.

194
00:28:22,870 --> 00:28:24,580
Come here.

195
00:28:27,958 --> 00:28:31,003
- Parsons.
- Yeah, from New Zealand.

196
00:28:31,169 --> 00:28:34,047
This man's putting
quite a collection together.

197
00:28:34,464 --> 00:28:36,133
Know him?

198
00:28:36,925 --> 00:28:38,176
Never saw him before.

199
00:28:39,678 --> 00:28:41,805
What do you know about this Han cat?

200
00:28:43,056 --> 00:28:45,601
Just rumors. I hear he likes to live big.

201
00:28:50,230 --> 00:28:52,274
They don't live so big over there.

202
00:28:52,441 --> 00:28:54,985
Ghettos are the same all over the world.

203
00:28:55,152 --> 00:28:57,446
They stink.

204
00:28:58,947 --> 00:29:00,741
Same old Williams.

205
00:29:02,576 --> 00:29:04,119
Yeah.

206
00:30:28,829 --> 00:30:30,956
What have we got here? A little action.

207
00:30:31,123 --> 00:30:35,627
Insects. Okay, I'll lay you 50 bucks
on the big one.

208
00:30:36,211 --> 00:30:38,880
Fifty. $50 on the big one, all right?

209
00:30:39,047 --> 00:30:40,632
I'll give you 5-to-1.

210
00:30:41,550 --> 00:30:42,968
Would you bet 100?

211
00:30:44,845 --> 00:30:46,096
You're on.

212
00:30:54,604 --> 00:30:56,648
Come on, let's end this thing quickly.

213
00:30:58,400 --> 00:31:00,861
Thattababy. Come on. Come on!

214
00:31:01,737 --> 00:31:05,741
That's it. Just get him on his back.
Get him on his back! Come on.

215
00:31:05,907 --> 00:31:09,369
Come on. Give up!
Come on! Hey.

216
00:31:09,911 --> 00:31:14,499
Oh, for crying out loud.
Would you believe that?

217
00:31:17,919 --> 00:31:20,172
Dumb shit.

218
00:32:12,265 --> 00:32:14,059
Do I bother you?

219
00:32:14,643 --> 00:32:16,645
Don't waste yourself.

220
00:32:18,063 --> 00:32:21,650
- What's your style?
- My style?

221
00:32:22,651 --> 00:32:25,487
You can call it the art of fighting
without fighting.

222
00:32:25,821 --> 00:32:28,323
"The art of fighting without fighting"?

223
00:32:29,157 --> 00:32:31,284
- Show me some of it.
- Later.

224
00:32:38,583 --> 00:32:40,335
All right.

225
00:32:42,212 --> 00:32:44,005
Don't you think we need more room?

226
00:32:46,341 --> 00:32:47,634
Where else?

227
00:32:48,844 --> 00:32:53,807
That island, on the beach.
We can take this boat.

228
00:33:01,356 --> 00:33:03,108
Okay.

229
00:33:16,413 --> 00:33:20,041
Hey, what the hell are you doing?
Hey, are you crazy?

230
00:33:22,252 --> 00:33:24,963
Pull me in! Pull me in!

231
00:33:26,715 --> 00:33:31,303
Don't try to pull yourself up
or I'll let go of the line.

232
00:34:31,738 --> 00:34:33,907
Would you look at that.

233
00:34:37,869 --> 00:34:42,082
A woman like that could teach you
a lot about yourself.

234
00:35:05,563 --> 00:35:07,274
Welcome.

235
00:36:04,748 --> 00:36:06,499
This way.

236
00:36:09,502 --> 00:36:13,381
I'll show you to your rooms.
The banquet begins promptly at 8.

237
00:36:13,548 --> 00:36:16,760
I think you'll find our little island
quite charming.

238
00:38:13,877 --> 00:38:17,964
Say, I hope you haven't spent all
that money you won from me yesterday.

239
00:38:18,131 --> 00:38:20,675
- I plan to win it back.
- Oh? How?

240
00:38:22,051 --> 00:38:24,804
You'll find out, after you've lost it.

241
00:38:25,430 --> 00:38:28,600
You seem to be very much
at home here, Mr. Roper.

242
00:38:29,142 --> 00:38:32,187
This guy Han's got a great sense
of hospitality.

243
00:38:32,353 --> 00:38:35,106
And a fantastic sense of style.

244
00:38:37,150 --> 00:38:38,818
Yeah, it's great.

245
00:38:38,985 --> 00:38:40,862
Then why are you so apprehensive?

246
00:38:41,112 --> 00:38:42,822
Apprehensive?

247
00:38:43,865 --> 00:38:47,827
No. I was just wondering
whether it was okay to drink the water.

248
00:38:48,286 --> 00:38:51,748
Mr. Roper, don't con me.

249
00:38:51,915 --> 00:38:53,208
Wanna bet?

250
00:39:03,593 --> 00:39:08,014
No. I really don't think so. Thanks.

251
00:39:08,389 --> 00:39:10,391
What's the matter, you on a diet?

252
00:39:10,558 --> 00:39:14,062
I'd like to eat, if I could
find something I could keep down.

253
00:39:14,812 --> 00:39:19,734
Well, I can't wait to meet our host.
I hear this is only one of his B parties.

254
00:39:20,235 --> 00:39:23,238
You ever been to
a martial arts tournament like this?

255
00:39:24,280 --> 00:39:26,115
Never

256
00:39:26,282 --> 00:39:29,577
I have a funny feeling we're being
fattened up for the kill.

257
00:39:31,746 --> 00:39:33,998
Better keep an eye out for the referee.

258
00:39:34,165 --> 00:39:36,584
- Know what I mean?
- Yeah.

259
00:40:12,370 --> 00:40:15,582
Gentlemen, welcome.

260
00:40:15,748 --> 00:40:18,293
You honor our island.

261
00:40:18,960 --> 00:40:22,130
I look forward to a tournament...

262
00:40:22,297 --> 00:40:25,633
...of truly epic proportions.

263
00:40:26,134 --> 00:40:28,595
We are unique, gentlemen...

264
00:40:28,761 --> 00:40:31,889
...in that we create ourselves...

265
00:40:32,056 --> 00:40:34,934
...through long years
of rigorous training...

266
00:40:35,101 --> 00:40:38,062
...sacrifice, denial, pain.

267
00:40:38,229 --> 00:40:43,318
We forge our bodies
in the fire of our will.

268
00:40:43,484 --> 00:40:46,779
But tonight, let us celebrate.

269
00:40:46,946 --> 00:40:52,785
Gentlemen, you have our gratitude.

270
00:42:00,019 --> 00:42:01,896
Mr. Williams.

271
00:42:11,823 --> 00:42:13,366
Mr. Williams!

272
00:42:22,166 --> 00:42:24,001
For me?

273
00:42:27,255 --> 00:42:29,090
You shouldn't have.

274
00:42:29,799 --> 00:42:31,551
But...

275
00:42:33,261 --> 00:42:35,471
I'll take you, darling.

276
00:42:38,266 --> 00:42:39,934
And you.

277
00:42:43,521 --> 00:42:45,440
And you.

278
00:42:45,815 --> 00:42:49,110
And you.

279
00:42:52,447 --> 00:42:55,783
Please understand, if I missed anyone...

280
00:42:56,159 --> 00:42:58,369
...it's been a big day.

281
00:42:58,536 --> 00:43:00,121
I'm a little tired.

282
00:43:01,372 --> 00:43:04,333
Oh, of course, Mr. Williams.

283
00:43:04,792 --> 00:43:08,171
You must conserve your strength.

284
00:43:10,798 --> 00:43:12,383
Come in.

285
00:43:17,221 --> 00:43:19,724
A gift, Mr. Lee.

286
00:43:27,982 --> 00:43:32,153
- If you don't see anything you like...
- There was a girl at the feast tonight.

287
00:43:32,320 --> 00:43:33,654
Which girl, sir?

288
00:43:35,281 --> 00:43:36,824
The owner of this dart.

289
00:43:38,659 --> 00:43:41,829
Oh, yes. I know the one.

290
00:43:42,538 --> 00:43:43,998
I'll send her to you.

291
00:43:51,798 --> 00:43:54,509
Another fine mess you got me into.

292
00:43:58,095 --> 00:43:59,722
Come in.

293
00:44:13,486 --> 00:44:15,238
Hi.

294
00:44:17,031 --> 00:44:19,617
Well, well, one more lovely than the next.

295
00:44:19,784 --> 00:44:21,661
What did you have in mind?

296
00:44:21,953 --> 00:44:23,704
Pick one.

297
00:44:26,374 --> 00:44:28,501
I already have.

298
00:44:30,878 --> 00:44:32,839
Wise decision.

299
00:44:45,393 --> 00:44:48,855
I wanna talk to you, Mei Ling.

300
00:44:50,565 --> 00:44:52,149
Where do you come from?

301
00:44:52,316 --> 00:44:54,819
- Braithwaite--
- Let's be quiet.

302
00:44:55,069 --> 00:44:56,362
Have you seen anything?

303
00:44:56,654 --> 00:45:01,033
Nothing much.
I'm kept in the palace, watched always.

304
00:45:01,367 --> 00:45:04,579
I know nothing of Han's activities
away from the palace.

305
00:45:05,121 --> 00:45:07,832
But I can tell you this: people disappear.

306
00:45:07,999 --> 00:45:11,252
- Who?
- The girls. Every one of them.

307
00:45:11,419 --> 00:45:15,673
They're summoned to Han's at night
and the next day they're gone.

308
00:45:17,258 --> 00:45:19,343
I know I don't have much time.

309
00:45:33,858 --> 00:45:37,528
You must attend the morning ritual
in uniform.

310
00:45:37,695 --> 00:45:39,614
Outside.

311
00:46:00,051 --> 00:46:03,304
Mr. Lee, why you no wear uniform?

312
00:47:02,530 --> 00:47:05,199
Gentlemen, let the tournament begin!

313
00:47:11,455 --> 00:47:13,165
Bolo.

314
00:48:44,048 --> 00:48:46,926
Good work. You made me some bread.

315
00:48:47,802 --> 00:48:49,345
- Mr. Roper.
- I'm ready.

316
00:48:49,512 --> 00:48:51,138
Okay.

317
00:48:53,349 --> 00:48:56,477
I got myself a real pigeon here.

318
00:48:56,644 --> 00:48:59,230
Keep the action going for me, will you?

319
00:48:59,647 --> 00:49:01,273
What do you think, Roper?

320
00:49:01,440 --> 00:49:06,821
Oh, no sweat.
Give my friend 8-to-3 all the way.

321
00:50:57,056 --> 00:50:59,516
I'm finding out about myself.

322
00:50:59,934 --> 00:51:02,770
This is the real me, definitely.

323
00:51:03,437 --> 00:51:04,939
Do you like it here?

324
00:51:05,105 --> 00:51:07,274
Oh, yeah. But a little lower.

325
00:51:07,858 --> 00:51:11,111
No. I mean here on the island.

326
00:51:11,612 --> 00:51:14,949
Yeah, I like it here.
But a little lower.

327
00:51:15,115 --> 00:51:17,618
A man like you belongs here.

328
00:51:24,667 --> 00:51:27,419
To you, I'm just another pretty face.

329
00:51:58,826 --> 00:52:00,995
Where you go?

330
00:52:01,161 --> 00:52:02,621
Out in the moonlight, baby.

331
00:52:02,997 --> 00:52:07,418
- It is not allowed. You must stay.
- Oh?

332
00:52:08,002 --> 00:52:09,962
Goodbye.

333
00:57:51,845 --> 00:57:53,513
A human fly.

334
00:57:57,142 --> 00:57:58,769
Gentlemen...

335
00:57:59,269 --> 00:58:01,772
...it seems that one of you...

336
00:58:02,314 --> 00:58:05,317
...was not content last night...

337
00:58:05,859 --> 00:58:10,030
...with the hospitality of the palace...

338
00:58:11,198 --> 00:58:16,870
...and sought diversion
elsewhere on the island.

339
00:58:17,537 --> 00:58:21,667
Who it was is not important...

340
00:58:21,833 --> 00:58:23,919
...at this time.

341
00:58:24,378 --> 00:58:27,547
What is important is that...

342
00:58:28,131 --> 00:58:34,096
...my guards performed
their duties incompetently.

343
00:58:34,262 --> 00:58:37,057
And now they must prove...

344
00:58:37,224 --> 00:58:42,229
...themselves worthy
to remain among us.

345
01:00:25,665 --> 01:00:28,543
Are you shocked, Mr. Williams?

346
01:00:29,711 --> 01:00:31,963
Only at how sloppy your man works.

347
01:00:36,343 --> 01:00:37,511
Mr. Lee?

348
01:00:39,679 --> 01:00:41,640
Are you ready?

349
01:01:20,178 --> 01:01:23,306
Boards don't hit back.

350
01:01:27,310 --> 01:01:29,938
- Bolo.
- Go!

351
01:02:18,069 --> 01:02:19,487
Oharra!

352
01:03:33,770 --> 01:03:35,188
Oharra!

353
01:04:36,332 --> 01:04:39,919
Oharra's treachery has disgraced us.

354
01:04:59,272 --> 01:05:02,150
If you wanna talk about this thing,
I'll be in my room.

355
01:05:02,317 --> 01:05:04,110
Gotcha.

356
01:05:07,614 --> 01:05:10,200
Williams? Williams.

357
01:05:10,366 --> 01:05:13,912
- Go to Han's study as soon as possible.
- All right.

358
01:05:18,208 --> 01:05:20,919
Hey, Roper.
I'll see you in half an hour

359
01:05:21,086 --> 01:05:22,712
Right.

360
01:05:31,137 --> 01:05:34,057
- Mr. Han?
- You fought well yesterday.

361
01:05:35,225 --> 01:05:37,477
Your style is unorthodox.

362
01:05:37,894 --> 01:05:39,020
But effective.

363
01:05:39,646 --> 01:05:43,483
It is not the art,
but the combat that you enjoy.

364
01:05:44,651 --> 01:05:46,444
The winning.

365
01:05:47,195 --> 01:05:49,739
We are all ready to win...

366
01:05:49,906 --> 01:05:53,743
...just as we are born
knowing only life.

367
01:05:55,453 --> 01:06:00,416
It is defeat that you must learn
to prepare for.

368
01:06:02,377 --> 01:06:04,295
I don't waste my time with it.

369
01:06:04,462 --> 01:06:07,423
When it comes, I won't even notice.

370
01:06:07,715 --> 01:06:09,217
Oh? How so?

371
01:06:09,884 --> 01:06:12,428
I'll be too busy looking good.

372
01:06:13,555 --> 01:06:16,558
What were you looking for
when you attacked my guards?

373
01:06:17,225 --> 01:06:18,643
Wasn't me.

374
01:06:18,810 --> 01:06:22,438
You were the only man
outside the palace.

375
01:06:23,273 --> 01:06:27,068
I was outside, but I wasn't the only one.

376
01:06:27,569 --> 01:06:29,571
You will tell me who else.

377
01:06:30,238 --> 01:06:34,284
Mr. Han, suddenly
I'd like to leave your island.

378
01:06:34,492 --> 01:06:38,580
- It is not possible.
- Bullshit, Mr. Han-Man!

379
01:06:44,961 --> 01:06:48,756
Man, you come right out of a comic book.

380
01:07:11,779 --> 01:07:14,115
Been practicing, huh?

381
01:09:00,430 --> 01:09:04,726
- Williams is expecting me in my room.
- I wanted to talk to you.

382
01:09:04,892 --> 01:09:07,770
We will meet your friend Williams later.

383
01:09:07,937 --> 01:09:11,482
- Okay.
- This is my museum.

384
01:09:12,233 --> 01:09:15,069
It is difficult
to associate these horrors...

385
01:09:15,236 --> 01:09:18,114
...with the proud civilizations
that created them:

386
01:09:18,281 --> 01:09:23,453
Sparta, Rome, the knights of Europe,
the samurai.

387
01:09:23,619 --> 01:09:27,415
They worship strength
because it is strength...

388
01:09:27,582 --> 01:09:30,668
...that makes all other values possible.

389
01:09:30,877 --> 01:09:33,880
Nothing survives without it.

390
01:09:34,088 --> 01:09:38,426
Who knows what delicate wonders
have died out of the world...

391
01:09:38,593 --> 01:09:41,471
...for want of the strength to survive.

392
01:09:41,679 --> 01:09:43,473
What's this?

393
01:09:48,144 --> 01:09:50,021
A souvenir.

394
01:10:00,990 --> 01:10:03,284
- Up here.
- A guillotine? No, thanks.

395
01:10:03,451 --> 01:10:06,037
This is the only angle I care
to see it from.

396
01:10:06,204 --> 01:10:08,664
If you please, Mr. Roper.

397
01:10:11,292 --> 01:10:13,461
You want me to put my head
on that thing?

398
01:10:14,462 --> 01:10:16,297
An act of faith.

399
01:10:18,424 --> 01:10:21,010
I'm a man of little faith, Mr. Han.

400
01:10:36,234 --> 01:10:40,571
Very few people can be totally ruthless.
It isn't easy.

401
01:10:40,738 --> 01:10:43,491
It takes more strength
than you might believe.

402
01:11:04,679 --> 01:11:06,889
Now you've got eight more.

403
01:11:14,730 --> 01:11:18,568
Then there is a point
you will not go beyond.

404
01:11:22,655 --> 01:11:24,532
Faked out again.

405
01:11:33,541 --> 01:11:35,543
This way.

406
01:11:46,179 --> 01:11:48,222
Our power plant.

407
01:12:00,318 --> 01:12:02,820
Oh, yeah.

408
01:12:03,404 --> 01:12:06,449
A lifetime of women, huh?

409
01:12:06,616 --> 01:12:09,243
A man's strength
can be measured by his appetites.

410
01:12:09,410 --> 01:12:13,247
Indeed, a man's strength flows
from his appetites.

411
01:12:13,414 --> 01:12:16,751
Oh, no. They are my daughters.

412
01:12:18,628 --> 01:12:20,630
Your daughters?

413
01:12:22,256 --> 01:12:26,844
Oh, I'm sorry. I thought that--
I misunderstood. Miss Han?

414
01:12:31,766 --> 01:12:35,102
And also,
they are my most personal guard.

415
01:12:35,603 --> 01:12:37,730
I admire your judgment.

416
01:12:37,897 --> 01:12:41,609
Nobody's as loyal as daddy's little girl.

417
01:12:42,360 --> 01:12:44,695
- Mr. Roper?
- No, thanks.

418
01:12:44,862 --> 01:12:47,782
I'll get up myself.

419
01:12:48,449 --> 01:12:49,951
Nice meeting you.

420
01:12:58,334 --> 01:13:00,336
Opium.

421
01:13:03,130 --> 01:13:07,301
We are investing in corruption, Mr. Roper.

422
01:13:07,468 --> 01:13:10,972
The business of corruption
is like any other.

423
01:13:11,138 --> 01:13:12,473
Oh, yeah.

424
01:13:12,640 --> 01:13:17,520
Provide your customers
with products they need...

425
01:13:17,687 --> 01:13:21,649
...and encourage that need a little bit
to stimulate your market...

426
01:13:21,816 --> 01:13:25,403
...pretty soon your customers
depend on you. I mean, really need you.

427
01:13:25,570 --> 01:13:27,655
It's the law of economics.

428
01:13:27,822 --> 01:13:29,490
Right.

429
01:13:29,657 --> 01:13:33,578
And here we are stimulating
quite another need.

430
01:13:50,678 --> 01:13:54,682
You wonder why I am exposing
so much of myself?

431
01:13:54,849 --> 01:13:58,102
I forget what I see very easily.

432
01:13:58,352 --> 01:13:59,562
But then, why are you?

433
01:14:00,187 --> 01:14:05,109
I'm hoping you'll join us,
represent us in the United States.

434
01:14:06,527 --> 01:14:10,531
I'm beginning to understand the thing
with the tournament. The whole setup.

435
01:14:11,073 --> 01:14:13,534
It's a great way
to recruit new talent, huh?

436
01:14:19,540 --> 01:14:21,375
And who are they?

437
01:14:22,043 --> 01:14:26,047
The refuse found in waterfront bars.

438
01:14:26,839 --> 01:14:28,841
Shanghaied?

439
01:14:29,634 --> 01:14:35,640
Just lost, drunken men who no longer care
where they find themselves each morning.

440
01:14:42,897 --> 01:14:47,068
You left some rather
sizable debts in America.

441
01:14:47,234 --> 01:14:50,738
Diners Club hasn't called in my card yet.

442
01:15:03,000 --> 01:15:06,587
There were some questions
which I was forced to ask.

443
01:15:06,879 --> 01:15:09,006
I got no answers.

444
01:15:29,276 --> 01:15:31,612
And you want me to join this?

445
01:15:31,779 --> 01:15:33,948
There are certain realities.

446
01:15:34,115 --> 01:15:38,452
I want us to have a clear understanding.

447
01:15:50,673 --> 01:15:52,341
No.

448
01:15:53,634 --> 01:15:56,137
There's no misunderstanding between us.

449
01:18:08,102 --> 01:18:11,605
Help me. Help, I'm 17!

450
01:18:11,939 --> 01:18:15,693
I'm from California.
Help me! Please, help me!

451
01:18:15,860 --> 01:18:19,822
Mister, come back, please!
You have to come back. Please!

452
01:18:19,989 --> 01:18:25,286
- Help me! Help me!
- Hey, quiet! Shut up!

453
01:24:19,640 --> 01:24:22,643
The battle with the guards
was magnificent.

454
01:24:23,310 --> 01:24:26,313
Your skill is extraordinary.

455
01:24:27,981 --> 01:24:30,984
And I was going to ask you to join us.

456
01:24:47,000 --> 01:24:51,338
My God. This came in half an hour ago.
Why didn't I--?

457
01:24:51,880 --> 01:24:54,883
Hello. Put your colonel on.

458
01:24:55,676 --> 01:24:57,678
Well, wake him up.

459
01:24:58,845 --> 01:25:02,266
I don't care if he's not alone.

460
01:25:04,017 --> 01:25:08,605
Damn it all, I don't care who he's with!
You bloody well put him on the line!

461
01:25:34,423 --> 01:25:39,052
Good morning, Mr. Roper.
We have been waiting for you.

462
01:25:40,053 --> 01:25:41,388
What's going on?

463
01:25:41,555 --> 01:25:45,809
Would you be good enough to participate
in this morning's edification?

464
01:25:46,101 --> 01:25:47,394
Edification.

465
01:25:53,442 --> 01:25:55,152
What are you gonna do to him?

466
01:25:55,402 --> 01:25:58,739
Not me, Mr. Roper. You.

467
01:26:01,575 --> 01:26:03,327
Bolo?

468
01:26:41,448 --> 01:26:43,450
Like you said...

469
01:26:43,825 --> 01:26:46,286
...there's a point I won't go beyond.

470
01:26:46,620 --> 01:26:48,789
I was right about you.

471
01:26:48,955 --> 01:26:53,293
We shall strive to be worthy
of your sense of grandeur.

472
01:26:53,627 --> 01:26:56,880
I will find someone with whom
you can fight.

473
01:26:57,089 --> 01:26:58,590
Bolo.

474
01:29:19,147 --> 01:29:20,732
Quick, go destroy them. Kill!

475
01:29:50,845 --> 01:29:52,681
Kill them!

476
01:29:58,144 --> 01:30:00,814
Kill! Fools!

477
01:32:48,314 --> 01:32:54,154
You have offended my family
and you have offended a Shaolin temple.

478
01:38:22,982 --> 01:38:24,901
<i>Remember...</i>

479
01:38:25,068 --> 01:38:28,196
<i>...the enemy has only images
and illusions...</i>

480
01:38:28,362 --> 01:38:32,658
<i>...behind which
he hides his true motives.</i>

481
01:38:33,076 --> 01:38:38,164
<i>Destroy the image
and you will break the enemy.</i>

482
01:42:27,560 --> 01:42:29,562
[ENGLISH]

482
01:42:30,305 --> 01:42:36,516
-= www.OpenSubtitles.org =-